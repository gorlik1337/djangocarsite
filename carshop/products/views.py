
from django.shortcuts import render
from django.forms import modelformset_factory
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.http import HttpResponseRedirect
from .forms import ImageForm, CarForm
from .models import Car, Images

@login_required
def post(request):

    ImageFormSet = modelformset_factory(Images,
                                        form=ImageForm, extra=3)
    #'extra' means the number of photos that you can upload   ^
    if request.method == 'POST':

        carForm = CarForm(request.POST)
        formset = ImageFormSet(request.POST, request.FILES,
                               queryset=Images.objects.none())


        if carForm.is_valid() and formset.is_valid():
            post_form = carForm.save(commit=False)
            post_form.user = request.user
            post_form.save()

            for form in formset.cleaned_data:
                #this helps to not crash if the user   
                #do not upload all the photos
                if form:
                    image = form['image']
                    photo = Images(car=post_form, image=image)
                    photo.save()
            messages.success(request,
                             "Dodano nowy pojazd to sklepu!")
            return HttpResponseRedirect("/admin/")
        else:
            print(carForm.errors, formset.errors)
    else:
        carForm = CarForm()
        formset = ImageFormSet(queryset=Images.objects.none())
    return render(request, 'addproductform.html',
                  {'carForm': carForm, 'formset': formset})

def car_view(request):
    id = None
    try:
        id = request.GET['id']
    except:
        return HttpResponseRedirect("/")
    #id = 17

    try:
        car = Car.objects.get(id=id)
    except:
        return HttpResponseRedirect("/")

    images=[]
    for img in Car.objects.get(id=id).images_set.all():
        images.append(img.image.url)
        #print(img.image.url)
    #Car.objects.get(id=17).images_set.all()[0].image.url

    return render(request, 'carinfo.html', {'car': car, 'images': images, "title": "RedCar Shop - "+car.marka+" "+car.model+" "+str(car.price)+"zl"})

def allCarsArray():
    out=[]
    for car in Car.objects.all():
        images=[]
        firstimg=None
        for img in car.images_set.all():
            images.append(img.image.url)
            firstimg = images[0]

        out.append({'car': car, 'images': images, 'firstimg': firstimg })
    return out