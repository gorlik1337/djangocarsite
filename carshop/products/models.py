from django.db import models

# Create your models here.
import datetime

class Car(models.Model):
    marka = models.CharField(max_length=120, help_text="Nazwa marki np. Audi")      #audi 
    
    model = models.CharField(max_length=120, help_text="Nazwa modelu np. Q7")       #q7

    YEAR_CHOICES = []
    for r in range(1960, (datetime.datetime.now().year+1)):
        YEAR_CHOICES.append((r,r))        
    year = models.IntegerField(choices=YEAR_CHOICES, default=datetime.datetime.now().year, help_text="Rocznik")

    price = models.IntegerField(help_text="Cena")     #80 000
    
    przebieg = models.IntegerField(help_text="Przebieg (w km)")   #200 000

    desc = models.TextField(help_text="Opis (możesz użyć html)")       #niemiec plakal html
    
    def __str__(self):
        return self.marka+" "+self.model+" "+str(self.year)

from slugify import slugify
def get_image_filename(instance, filename):
    #title = instance.post.title
    #slug = slugify(title)
    slug = slugify(filename)
    return "static/carimg/%s-%s" % (slug, filename)  


class Images(models.Model):
    car = models.ForeignKey(Car, default=None, on_delete=None)
    image = models.ImageField(upload_to=get_image_filename,
                              verbose_name='Image')
    def __str__(self):
        return self.car.marka+" "+self.car.model+" "+str(self.car.year)

    #images = [link, link, link]