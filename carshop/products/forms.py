from .models import Car, Images

from django import forms
import datetime
class CarForm(forms.ModelForm):
    marka = forms.CharField(max_length=120, label="Nazwa marki np. Audi") 
    
    model = forms.CharField(max_length=120, label="Nazwa modelu np. Q7")

    YEAR_CHOICES = []
    for r in range(1960, (datetime.datetime.now().year+1)):
        YEAR_CHOICES.append((r,r))        
    year = forms.IntegerField(widget=forms.Select(choices=YEAR_CHOICES), label="Rocznik")

    price = forms.IntegerField(label="Cena") 
    
    przebieg = forms.IntegerField(label="Przebieg (w km)")

    desc = forms.CharField(widget=forms.Textarea, label="Opis (możesz użyć html)")
    

    class Meta:
        model = Car
        fields = ('marka', 'model', 'year', 'price', 'przebieg', 'desc' )


class ImageForm(forms.ModelForm):
    image = forms.ImageField(label='Image')    
    class Meta:
        model = Images
        fields = ('image', )