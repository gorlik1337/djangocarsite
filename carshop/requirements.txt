Django==2.2.3
Pillow==6.1.0
python-slugify==3.0.3
pytz==2019.1
sqlparse==0.3.0
text-unidecode==1.2