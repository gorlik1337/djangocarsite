from django.db import models

# Create your models here.

class msg(models.Model):
    email = models.EmailField(max_length=254)
    message = models.TextField()
    def __str__(self):
        return str(self.id)+". ["+self.email+"]: "+self.message[:500]
    