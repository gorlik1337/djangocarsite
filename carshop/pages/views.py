from django.shortcuts import render
from django.http import HttpResponse
from products.views import allCarsArray

from .forms import KontaktForm

# Create your views here.

def home_view(request):
    # return HttpResponse("<h1>home!</h1>")
    allCars = allCarsArray()
    vardb = {"title": "RedCar Shop - Strona Główna",
        "navbar_home": "active", "shop": allCars,
    }
    return render(request, "home.html", vardb)


def kontakt_view(request):
    form = KontaktForm(request.POST)
    formsuccess = False
    if form.is_valid():
        form.save()
        formsuccess = True

    vardb = {"title": "RedCar Shop - Kontakt",
        "navbar_kontakt": "active",
        "form": form, 
        "formsuccess": formsuccess,
    }
    return render(request, "kontakt.html", vardb)

def onas_view(request):    
    vardb = {"title": "RedCar Shop - Strona Główna",
        "navbar_onas": "active",
    }
    return render(request, "onas.html", vardb)
