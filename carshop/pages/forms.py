from django import forms
from .models import msg

class KontaktForm(forms.ModelForm):
    class Meta:
        model = msg
        fields = [
            'email', 'message',
        ]
